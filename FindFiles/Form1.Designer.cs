﻿namespace FindFiles
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonStart = new System.Windows.Forms.Button();
            this.treeViewFiles = new System.Windows.Forms.TreeView();
            this.textBoxSearchingText = new System.Windows.Forms.TextBox();
            this.textBoxTemplate = new System.Windows.Forms.TextBox();
            this.textBoxDirectoryName = new System.Windows.Forms.TextBox();
            this.buttonGetDirectory = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonPause = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxCurrentFile = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxTimer = new System.Windows.Forms.TextBox();
            this.mainTimer = new System.Windows.Forms.Timer(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxCountFiles = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStart.Location = new System.Drawing.Point(713, 11);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 53);
            this.buttonStart.TabIndex = 5;
            this.buttonStart.Text = "Начать новый поиск";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // treeViewFiles
            // 
            this.treeViewFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeViewFiles.Location = new System.Drawing.Point(13, 13);
            this.treeViewFiles.Name = "treeViewFiles";
            this.treeViewFiles.Size = new System.Drawing.Size(267, 440);
            this.treeViewFiles.TabIndex = 1;
            this.treeViewFiles.TabStop = false;
            // 
            // textBoxSearchingText
            // 
            this.textBoxSearchingText.Location = new System.Drawing.Point(488, 11);
            this.textBoxSearchingText.Name = "textBoxSearchingText";
            this.textBoxSearchingText.Size = new System.Drawing.Size(207, 20);
            this.textBoxSearchingText.TabIndex = 0;
            // 
            // textBoxTemplate
            // 
            this.textBoxTemplate.Location = new System.Drawing.Point(488, 49);
            this.textBoxTemplate.Name = "textBoxTemplate";
            this.textBoxTemplate.Size = new System.Drawing.Size(207, 20);
            this.textBoxTemplate.TabIndex = 2;
            // 
            // textBoxDirectoryName
            // 
            this.textBoxDirectoryName.Location = new System.Drawing.Point(488, 89);
            this.textBoxDirectoryName.Name = "textBoxDirectoryName";
            this.textBoxDirectoryName.Size = new System.Drawing.Size(207, 20);
            this.textBoxDirectoryName.TabIndex = 3;
            // 
            // buttonGetDirectory
            // 
            this.buttonGetDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGetDirectory.Location = new System.Drawing.Point(488, 127);
            this.buttonGetDirectory.Name = "buttonGetDirectory";
            this.buttonGetDirectory.Size = new System.Drawing.Size(75, 23);
            this.buttonGetDirectory.TabIndex = 4;
            this.buttonGetDirectory.Text = "Обзор";
            this.buttonGetDirectory.UseVisualStyleBackColor = true;
            this.buttonGetDirectory.Click += new System.EventHandler(this.buttonGetDirectory_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(394, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Искомый текст*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(396, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Формат файла*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(449, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Путь*";
            // 
            // buttonPause
            // 
            this.buttonPause.Location = new System.Drawing.Point(713, 70);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(75, 23);
            this.buttonPause.TabIndex = 6;
            this.buttonPause.Text = "Пауза";
            this.buttonPause.UseVisualStyleBackColor = true;
            this.buttonPause.Click += new System.EventHandler(this.buttonPause_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(311, 227);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Текущий файл";
            // 
            // textBoxCurrentFile
            // 
            this.textBoxCurrentFile.Location = new System.Drawing.Point(417, 227);
            this.textBoxCurrentFile.Multiline = true;
            this.textBoxCurrentFile.Name = "textBoxCurrentFile";
            this.textBoxCurrentFile.ReadOnly = true;
            this.textBoxCurrentFile.Size = new System.Drawing.Size(371, 42);
            this.textBoxCurrentFile.TabIndex = 8;
            this.textBoxCurrentFile.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(288, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Пройденное время";
            // 
            // textBoxTimer
            // 
            this.textBoxTimer.Location = new System.Drawing.Point(417, 275);
            this.textBoxTimer.Name = "textBoxTimer";
            this.textBoxTimer.ReadOnly = true;
            this.textBoxTimer.Size = new System.Drawing.Size(67, 20);
            this.textBoxTimer.TabIndex = 8;
            this.textBoxTimer.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(506, 278);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Файлов обработанно";
            // 
            // textBoxCountFiles
            // 
            this.textBoxCountFiles.Location = new System.Drawing.Point(628, 275);
            this.textBoxCountFiles.Name = "textBoxCountFiles";
            this.textBoxCountFiles.ReadOnly = true;
            this.textBoxCountFiles.Size = new System.Drawing.Size(67, 20);
            this.textBoxCountFiles.TabIndex = 8;
            this.textBoxCountFiles.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 462);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxCountFiles);
            this.Controls.Add(this.textBoxTimer);
            this.Controls.Add(this.textBoxCurrentFile);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonPause);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxDirectoryName);
            this.Controls.Add(this.textBoxTemplate);
            this.Controls.Add(this.textBoxSearchingText);
            this.Controls.Add(this.treeViewFiles);
            this.Controls.Add(this.buttonGetDirectory);
            this.Controls.Add(this.buttonStart);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TreeView treeViewFiles;
        private System.Windows.Forms.TextBox textBoxSearchingText;
        private System.Windows.Forms.TextBox textBoxTemplate;
        private System.Windows.Forms.TextBox textBoxDirectoryName;
        private System.Windows.Forms.Button buttonGetDirectory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonPause;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCurrentFile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxTimer;
        private System.Windows.Forms.Timer mainTimer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxCountFiles;
    }
}

