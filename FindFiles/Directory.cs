﻿using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;

namespace FindFiles
{
    public class MyDirectory
    {
        private string _path;
        private string _name;
        private string[] _files;
        public MyDirectory[] Directories;


        public delegate void ChangeLevel(MyDirectory directory);
        public static event ChangeLevel LevelUp;
        public static event ChangeLevel LevelDown;

        public MyDirectory(string path)
        {
            var info = new DirectoryInfo(path);
            _name = info.Name;
            _path = path;
        }

        public string GetPath()
        {
            return _path;
        }

        public string GetName()
        {
            return _name;
        }
        private void GetFiles()
        {

            _files = Directory.GetFiles(_path);
            for (int i = 0; i < _files.Length; i++)
            {
                var fileInfo = new FileInfo(_files[i]);
                _files[i] = fileInfo.Name;
            }
        }

        public void GetDirectories()
        {
            string[] items = Directory.GetDirectories(_path);
            Directories = new MyDirectory[items.Length];

            for (var i = 0; i < items.Length; i++)
            {
                Directories[i] = new MyDirectory(items[i]);
            }
        }

        public void Search(string text, string template)
        {
            while (Searcher.IsPause)
            {
                Thread.Sleep(1);
            }
            GetFiles();
            foreach (var file in _files)
            {
                var fileFullPath = _path + "\\" + file;
                Searcher.ChangeProcessingFileName(fileFullPath);
                if (CheckFileName(fileFullPath, template))
                {
                    if (CheckFile(text, fileFullPath))
                    {
                        Searcher.AddFoundFileName(file);
                    }
                }

            }

            GetDirectories();
            foreach (var directory in Directories)
            {
                LevelDown(directory);
                directory.Search(text, template);
                LevelUp(directory);
            }
        }

        public bool CheckFile(string text, string path)
        {
            string[] file = File.ReadAllLines(path);
            bool isLineHasText = false;
            foreach (string line in file)
            {
                isLineHasText = line.Contains(text);
                if (isLineHasText)
                {
                    break;
                }
            }
            return isLineHasText;
        }

        private bool CheckFileName(string fileName, string template)
        {
            return Regex.IsMatch(fileName, template, RegexOptions.IgnoreCase);
        }
    }
}
