﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace FindFiles
{
    [Serializable]
    class SearchData
    {
        public string Text { get; set; }
        public string Template { get; set; }
        public string Directory { get; set; }

        public static void SaveTo(string path,SearchData sd )
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (Stream stream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
            {
                bf.Serialize(stream, sd);
            }
        }

        public static SearchData  LoadFrom(string path)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
               var temp =  bf.Deserialize(stream) as SearchData;
               return temp;
            }
        }
    }
}
