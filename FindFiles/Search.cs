﻿using System;
using System.IO;
using System.Collections.Generic;

namespace FindFiles
{
    public static class Searcher
    {
        public static bool IsPause { get; set; }
        public static string CurrentFileName
        {
            get
            {
                return _currentFileName;
            }
            set
            {
                _currentFileName = value;
                ChangeCurrentFileName(_currentFileName);
            }
        }
        private static List<string> _foundFiles;
        private static string _currentFileName;
        private static MyDirectory _directory;


        public delegate void File(string filename);

        public static event File FileIsFound;
        public static event File ChangeCurrentFileName;

        static Searcher()
        {
            IsPause = false;
            _foundFiles = new List<string>();
        }

        public static void Search(string text,string template,string path)
        {
            template = "^.*\\." + template +"$";
            _directory = new MyDirectory(path);
            _directory.Search(text, template);
           
        }

        public static void AddFoundFileName(string fileName)
        {
            _foundFiles.Add(fileName);
            FileIsFound(fileName);
        }

        public static void ChangeProcessingFileName(string fileName)
        {
            CurrentFileName = fileName;
        }
    }
}
