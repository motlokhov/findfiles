﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FindFiles
{
    public partial class Form1 : Form
    {
        private dynamic currentNode;
        private delegate void ThreadSearcher(string text, string template, string directory);
        private delegate void Level(MyDirectory directory);
        private static Thread thread;
        private SearchData _searchData;
        private byte sec = 0;
        private byte min = 0;
        private uint countFiles = 0;
        public Form1()
        {
            InitializeComponent();
            Searcher.FileIsFound += Searcher_FileIsFound;
            Searcher.ChangeCurrentFileName += Searcher_ChangeCurrentFileName;
            MyDirectory.LevelDown += MyDirectory_LevelDown;
            MyDirectory.LevelUp += MyDirectory_LevelUp;

            if(new FileInfo("data.dat").Exists)
            {
                _searchData = SearchData.LoadFrom("data.dat");
                textBoxTemplate.Text = _searchData.Template;
                textBoxSearchingText.Text = _searchData.Text;
                textBoxDirectoryName.Text = _searchData.Directory;
            }
            else
            {
                _searchData = new SearchData();
            }
        }

        private void Searcher_ChangeCurrentFileName(string filename)
        {
            countFiles++;
            Invoke((Action)(() =>
            {
                textBoxCountFiles.Text = countFiles.ToString();
                textBoxCurrentFile.Text = filename;
                Update();
            }));
        }

        private void MyDirectory_LevelUp(MyDirectory directory)
        {
            Invoke((Action)(() =>
            {
                var tempNode = currentNode;
                currentNode = currentNode.Parent;
                currentNode.Expand();
                
                if (tempNode.Nodes.Count == 0)
                {
                    tempNode.Remove();
                }
                Update();
            }));
        }

        private void MyDirectory_LevelDown(MyDirectory directory)
        {
            
            Invoke((Action)(() =>
            {
                textBoxCurrentFile.Text = directory.GetPath();
                currentNode.Nodes.Add( new TreeNode(directory.GetName()));
                currentNode = currentNode.Nodes[currentNode.Nodes.Count-1];
                Update();
            }));
            
        }

        private void Searcher_FileIsFound(string filename)
        {
            Invoke((Action)(() =>
            {
                
                currentNode.Nodes.Add(filename);
                Update();
            }));
            
           
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
           _searchData.Text = textBoxSearchingText.Text;
           _searchData.Template = textBoxTemplate.Text;
           _searchData.Directory = textBoxDirectoryName.Text;

            if (string.IsNullOrEmpty(_searchData.Text) || string.IsNullOrEmpty(_searchData.Template) || string.IsNullOrEmpty(_searchData.Directory))
            {
                MessageBox.Show("Обязательные поля не заполненны (поля со звездочкой)");
                return;
            }

            countFiles = 0;
            textBoxCountFiles.Text = countFiles.ToString();
            textBoxTimer.Text = "00 : 00";
            Searcher.IsPause = false;
            buttonPause.Text = "Пауза";

            ReloadTimer();
            SearchData.SaveTo("data.dat",_searchData);
            treeViewFiles.Nodes.Clear();
            treeViewFiles.Nodes.Add(new TreeNode(textBoxDirectoryName.Text));
            currentNode = treeViewFiles.Nodes[0];
            ReloadThread();
        }

        private void ReloadTimer()
        {
            sec = 0;
            min = 0;
            mainTimer.Tick -= MainTimer_Tick;
            mainTimer = new System.Windows.Forms.Timer();
            mainTimer.Interval = 1000;
            mainTimer.Start();
            mainTimer.Tick += MainTimer_Tick;
            
        }

        private void ReloadThread()
        {
            if(thread != null)
            {
                thread.Abort();
            }
            thread = new Thread(new ThreadStart(StartSearch));
            thread.IsBackground = true;
            thread.Start();
        }

        private void MainTimer_Tick(object sender, EventArgs e)
        {
            sec++;
            if (sec > 59)
            {
                sec = 0;
                min++;
            }
            if (!thread.IsAlive)
            {
                mainTimer.Stop();
            }
            Invoke((Action)(() => 
            {
                
                textBoxTimer.Text = (min < 10 ? "0" + min:min.ToString()) + " : "+ (sec<10?"0" + sec :sec.ToString());
            }));

            
        }

        private void StartSearch()
        {
            Searcher.Search(textBoxSearchingText.Text, textBoxTemplate.Text, textBoxDirectoryName.Text);
        }

        private void buttonGetDirectory_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog fb = new FolderBrowserDialog())
            {
                if (fb.ShowDialog() == DialogResult.OK)
                {
                    textBoxDirectoryName.Text = fb.SelectedPath;
                }
            }
        }


        private void buttonPause_Click(object sender, EventArgs e)
        {
            if (!Searcher.IsPause)
            {
                buttonPause.Text = "Продолжить";
                Searcher.IsPause = true;
                mainTimer.Stop();
            }
            else
            {
                buttonPause.Text = "Пауза";
                Searcher.IsPause = false;
                mainTimer.Start();
            }
        }


    }
}
